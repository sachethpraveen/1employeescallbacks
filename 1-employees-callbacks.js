const fs = require("fs");
const path = require("path");

// 1. Retrieve data for ids : [2, 13, 23].

function retrieveData(array, filePath) {
  fs.readFile(path.join(__dirname, filePath), (err, data) => {
    if (err) {
      console.log("Error:", err);
    } else {
      fs.writeFile(
        path.join(__dirname, "output1.json"),
        JSON.stringify(
          JSON.parse(data)["employees"].filter((employee) => {
            return array.includes(employee.id);
          })
        ),
        () => {}
      );
    }
  });
}

/* 2. Group data based on companies.
{ "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []} */

function groupData(filePath, callback) {
  fs.readFile(path.join(__dirname, filePath), (err, data) => {
    if (err) {
      console.log("Error:", err);
    } else {
      const groupedData = JSON.parse(data)["employees"].reduce(
        (groups, employee) => {
          if (groups.hasOwnProperty(employee.company)) {
            groups[employee.company].push(employee);
          } else {
            groups[employee.company] = [employee];
          }
          return groups;
        },
        {}
      );

      if (callback === undefined) {
        console.log(groupedData);
        fs.writeFile(
          path.join(__dirname, "output2.json"),
          JSON.stringify(groupedData),
          () => {}
        );
      } else {
        callback(err, groupedData);
      }
    }
  });
}

// 3. Get all data for company Powerpuff Brigade
function getData(filepath, company) {
  groupData(filepath, (err, groupedData) => {
    if (err) {
      throw err;
    } else {
      fs.writeFile(
        path.join(__dirname, "output3.json"),
        JSON.stringify(groupedData[company]),
        () => {}
      );
    }
  });
}

getData("./data.json", "Powerpuff Brigade");

// 4. Remove entry with id 2.
function removeId(id, filePath) {
  fs.readFile(path.join(__dirname, filePath), (err, data) => {
    if (err) {
      console.log("Error:", err);
    } else {
      data = JSON.parse(data)["employees"].filter((employee) => {
        return employee.id !== id;
      });

      fs.writeFile(
        path.join(__dirname, "output4.json"),
        JSON.stringify(data),
        () => {}
      );
    }
  });
}

//5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
function sortData(filepath) {
  fs.readFile(path.join(__dirname, filepath), (err, data) => {
    if (err) {
      console.log("Error:", err);
    } else {
      data = JSON.parse(data);
      data.employees.sort((currentEmployee, nextEmployee) => {
        if (currentEmployee !== nextEmployee.company) {
          return currentEmployee.company.localeCompare(nextEmployee.company);
        } else {
          return currentEmployee.id - nextEmployee.id;
        }
      });
      fs.writeFile(
        path.join(__dirname, "output5.json"),
        JSON.stringify(data),
        () => {}
      );
    }
  });
}

sortData("./data.json");
